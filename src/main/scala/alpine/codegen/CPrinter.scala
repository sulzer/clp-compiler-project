package alpine
package codegen

import alpine.ast
import alpine.symbols
import alpine.symbols.Entity.builtinModule
import alpine.util.FatalError

import scala.annotation.tailrec
import scala.collection.mutable
import alpine.symbols.Type
import alpine.ast.Typecast
import alpine.ast.ErrorTree
import alpine.ast.Binding
import alpine.ast.ValuePattern
import alpine.ast.RecordPattern
import alpine.ast.Wildcard
import alpine.ast.Labeled
import alpine.driver.typeCheck
import alpine.ast.Identifier
import alpine.ast.IntegerLiteral
import alpine.ast.Literal
import alpine.ast.Selection
import alpine.ast.Application
import alpine.ast.PrefixApplication
import alpine.ast.InfixApplication
import alpine.ast.Conditional
import alpine.ast.Match
import alpine.ast.Let
import alpine.ast.Lambda
import alpine.ast.ParenthesizedExpression
import alpine.ast.AscribedExpression
import scala.annotation.meta.field

/** The transpilation of an Alpine program to C. */
final class CPrinter(syntax: TypedProgram) extends ast.TreeVisitor[CPrinter.Context, Unit]:

  import CPrinter.Context

  /** The program being evaluated. */
  private given TypedProgram = syntax

  private val header = "#include \"c_rt/rt.c\"\n"

  /** Returns a C program equivalent to `syntax`. */
  def transpile(): String =
    given c: Context = Context()
    syntax.declarations.foreach(_.visit(this))
    val b = StringBuilder() 
    c.swappingOutputBuffer(b) {
      context => context.typesToEmit.map(emitType(_)(using context))
    }
    val helperFunctions = c.helperFunctions.mkString
    header ++ b.toString() ++ helperFunctions ++ c.output.toString


  private def emitType(t: Type.Record | Type.Sum)(using context: Context): Unit =
    t match
      case r: Type.Record => emitRecord(r)
      case s: Type.Sum => emitSum(s)
    
  private def emitSum(t: Type.Sum)(using context: Context): Unit =
    val typeName = discriminator(t)
    context.output ++= s"typedef struct _${typeName}{\n"
    context.indentation += 1
    context.indent()
    context.output ++= s"union {\n"
    t.members.foreach { f =>
      context.indent()
      context.output ++= s"${transpiledType(f)} ${payloadField(f)};\n"
    }
    context.indent()
    context.output ++= "} payload;\n"
    context.indent()
    context.output ++= "int tag;\n"
    context.indentation -= 1
    context.output ++= s"} ${typeName};\n"


  /** Writes the C declaration of `t` in `context`. */
  private def emitRecord(t: symbols.Type.Record)(using context: Context): Unit =
    if (!t.fields.isEmpty) then
      emitNonSingletonRecord(t)
    else
      val recordName = discriminator(t)
      context.output ++= s"typedef struct _${recordName} ${recordName};\n"

  /** Writes the C declaration of `t`, which is not a singleton, in `context`. */
  private def emitNonSingletonRecord(t: symbols.Type.Record)(using context: Context): Unit =
    val recordName = discriminator(t)
    val b = StringBuilder(s"typedef struct _${recordName} {\n")
    var count  = 0
    t.fields.map(
      field => 
        // TODO currently using the label as the name of the field, maybe there's a better way
        // the name of the field must be synchronized with `transpiledReferenceTo` when the entity is a field.
        val name = field.label.getOrElse(s"_${count}")
        count += 1
        b ++= s"\t${transpiledType(field.value)} ${name};\n"
    )
    b ++= s"} ${recordName};\n"
    context.output ++= b.toString()

  /** Returns the transpiled form of `t`. */
  private def transpiledType(t: symbols.Type)(using context: Context): String =
    t match
      case u: symbols.Type.Builtin =>
        transpiledBuiltin(u)
      case u: symbols.Type.Record =>
        transpiledRecord(u)
      case u: symbols.Type.Arrow =>
        transpiledArrow(u)
      case u: symbols.Type.Sum =>
        transpiledSum(u)
      case _ => throw Error(s"type '${t}' is not representable in C")

  /** Returns the transpiled form of `t`. */
  private def transpiledBuiltin(t: symbols.Type.Builtin)(using context: Context): String =
    t match
      case symbols.Type.Bool => "bool"
      case symbols.Type.Int => "int"
      case symbols.Type.Float => "float"
      case symbols.Type.String => "char *"
      case symbols.Type.BuiltinModule => throw Error(s"type '${t}' is not representable in C")
      case symbols.Type.Any => ???

  /** Returns the transpiled form of `t`. */
  private def transpiledRecord(t: symbols.Type.Record)(using context: Context): String =
    t match
      case symbols.Type.Unit => "void"
      case _ =>
        context.registerUse(t)
        discriminator(t)

  /** Returns the transpiled form of `t`. */
  private def transpiledArrow(t: symbols.Type.Arrow)(using context: Context): String =
    ???

  /** Returns the transpiled form of `t`. */
  private def transpiledSum(t: symbols.Type.Sum)(using context: Context): String =
    context.registerUse(t)
    discriminator(t)


  private def payloadField(t: symbols.Type): String =
    discriminator(t) + "_Val"

  private def tagValue(t: symbols.Type, initType: symbols.Type): Int = 
    t match
      case u: symbols.Type.Sum => 
        u.members.indexWhere(_ == initType)
      case _ => throw new Error(s"Expected a union type but found '${t}'")


  /** Returns a string uniquely identifiyng `t` for use as a discriminator in a mangled name. */
  private def discriminator(t: symbols.Type): String =
    t match
      case u: symbols.Type.Builtin =>
        discriminator(u)
      case u: symbols.Type.Meta =>
        s"M${discriminator(u.instance)}"
      case u: symbols.Type.Definition =>
        "D" + u.identifier
      case u: symbols.Type.Record =>
        discriminator(u)
      case u: symbols.Type.Arrow =>
        discriminator(u)
      case u: symbols.Type.Sum =>
        discriminator(u)
      case _ =>
        throw Error(s"unexpected type '${t}'")

  /** Returns a string uniquely identifiyng `t` for use as a discriminator in a mangled name. */
  private def discriminator(t: symbols.Type.Builtin): String =
    t match
      case symbols.Type.Bool => "B"
      case symbols.Type.Int => "I"
      case symbols.Type.Float => "F"
      case symbols.Type.String => "S"
      case symbols.Type.BuiltinModule => "Z"
      case symbols.Type.Any => "A"

  /** Returns a string uniquely identifiyng `t` for use as a discriminator in a mangled name. */
  private def discriminator(t: symbols.Type.Record): String =
    val b = StringBuilder("R")
    b ++= t.identifier.drop(1)
    for f <- t.fields do 
      b ++= "_"
      b ++= f.label.getOrElse("")
      b ++= discriminator(f.value)
    b.toString()

  /** Returns a string uniquely identifiyng `t` for use as a discriminator in a mangled name. */
  private def discriminator(t: symbols.Type.Arrow): String =
    val b = StringBuilder("X")
    for i <- t.inputs do
      b ++= i.label.getOrElse("")
      b ++= discriminator(i.value)
    b ++= discriminator(t.output)
    b.toString

  /** Returns a string uniquely identifiyng `t` for use as a discriminator in a mangled name. */
  private def discriminator(t: symbols.Type.Sum): String =
    if t.members.isEmpty then "N" else
      "E" + t.members.map(discriminator).mkString

  /** Returns a transpiled reference to `e`. */
  private def transpiledReferenceTo(e: symbols.Entity): String =
    e match
      case symbols.Entity.Builtin(n, _) => s"${n.identifier}"
      case symbols.Entity.Declaration(n, t) => c_ified(n) + discriminator(t)
      case symbols.Entity.Field(whole, index) =>
        val field = whole.fields(index)
        field.label.getOrElse(s"_${index}")

  /** Returns a string representation of `n` suitable for use as a C identifier. */
  private def c_ified(n: symbols.Name): String =
    n.qualification match
      case Some(q) =>
        s"${c_ified(q)}_${n.identifier}"
      case None =>
        "_" + n.identifier

  override def visitLabeled[T <: ast.Tree](n: ast.Labeled[T])(using context: Context): Unit =
    unexpectedVisit(n)

  private def nameForBinding(variableName: String, variableType: Type, initializerType: Type) =
    variableType match
      case _: Type.Sum => s"${variableName}.payload.${payloadField(initializerType)}"
      case _ => variableName
    

  override def visitBinding(n: ast.Binding)(using context: Context): Unit =
    if (n.identifier == "main")
      // We are reading the main function declaration
      context.output ++= "int main() {\n"
      context.indentation += 1

      // Initialize all global variables
      context.output ++= "//----------------------------\n"
      context.output ++= "// Initialize global variables\n"
      context.output ++= context.globals
      context.globals.clear() // not necessary, purely symbolic
      context.output ++= "//----------------------------\n"

      // In this case, the initializer is the body of the function.
      // It should not be `None` so we `get` it without further checks.
      context.inScope(n.initializer.get.visit(this)(using _))

      context.indent()
      context.indentation -= 1
      context.output ++= "return 0;\n}"
    else
      // Any other binding
      val tpe = transpiledType(n.tpe)
      val name = c_ified(n.nameDeclared) + discriminator(n.tpe)
      // First declare the variable without initializing it
      context.indent()
      context.output ++= s"${tpe} ${name};\n"


      // If there is an initializer, visit it with this binding in the context
      n.initializer.foreach { expr =>
        val bindingName = nameForBinding(name, n.tpe, expr.tpe)
        val initType = discriminator(expr.tpe)
        val initialization = context.writingToString { c =>
          c.withPendingBinding(bindingName, Option.unless(expr.tpe.isBuiltin)(initType))(expr.visit(this)(using _))
        }
        // If this is a top level binding, add the initializer part to the globals, otherwise add it to the output buffer
        if (context.isTopLevel) context.globals ++= initialization else context.output ++= initialization
      }

  override def visitTypeDeclaration(n: ast.TypeDeclaration)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitFunction(n: ast.Function)(using context: Context): Unit =
    val out = n.tpe match
      case Type.Arrow(i, o) => o
      case _: Type => Type.Unit

    val name = c_ified(n.nameDeclared) + discriminator(n.tpe)
    // Not really necessary to clear pending bindings, as functions can only be top-level, so there shouldn't be any pending binding
    val parameters = context.clearingPendingBinding { context =>
      n.inputs.map { param =>
        context.writingToString(param.visit(this)(using _))
      }
    }

    val returnType = transpiledType(out)
    // Function header
    context.output ++= s"${returnType} ${name}("
    context.output.appendCommaSeparated(parameters)(_ ++= _)
    context.output ++= ") {\n"
    context.indentation += 1

    // Function body
    out match
      case Type.Unit => 
        // No return value
        n.body.visit(this)
        context.indent()
        context.output ++= "return;\n"
      case _ =>
        // No embedded functions means only one return value at time, so no need for unique names
        // TODO consider lambdas
        context.indent()
        context.output ++= s"${returnType} Ret;\n"
        // Visit with "Ret" as the pending binding
        context.withPendingBinding("Ret", Option.unless(out.isBuiltin)(returnType))(n.body.visit(this)(using _))
        context.indent()
        context.output ++= "return Ret;\n"

    // Function closing
    context.indentation -= 1
    context.output ++= "}\n"

  override def visitParameter(n: ast.Parameter)(using context: Context): Unit =
    val tpe = transpiledType(n.tpe)
    val name = c_ified(n.nameDeclared) + discriminator(n.tpe)
    context.output ++= s"${tpe} ${name}"

  override def visitIdentifier(n: ast.Identifier)(using context: Context): Unit =
    context.consideringInitialization(_.output ++= transpiledReferenceTo(n.referredEntity.get.entity))

  override def visitBooleanLiteral(n: ast.BooleanLiteral)(using context: Context): Unit =
    context.consideringInitialization(_.output ++= n.value)

  override def visitIntegerLiteral(n: ast.IntegerLiteral)(using context: Context): Unit =
    context.consideringInitialization(_.output ++= n.value)

  override def visitFloatLiteral(n: ast.FloatLiteral)(using context: Context): Unit =
    context.consideringInitialization(_.output ++= n.value)

  override def visitStringLiteral(n: ast.StringLiteral)(using context: Context): Unit =
    context.consideringInitialization(_.output ++= n.value)

  override def visitRecord(n: ast.Record)(using context: Context): Unit =
    context.consideringInitialization { context => 
      context.output ++= "{"
      val fs = n.fields.map(_.value).map { field =>
        context.writingToString(field.visit(this)(using _))
      }
      context.output.appendCommaSeparated(fs)(_ ++= _)
      context.output ++= "}"
    }

  override def visitSelection(n: ast.Selection)(using context: Context): Unit =
    context.consideringInitialization { context =>
      n.qualification.visit(this)(using context)
      context.output ++= s".${transpiledReferenceTo(n.referredEntity.get.entity)}"
    }

  override def visitApplication(n: ast.Application)(using context: Context): Unit =
    context.consideringInitialization { context => 
      // If the function returns Unit, we assume it's an independent instruction
      // This will fail if we try to execute an instruction within an initializer (which shouldn't be possible in alpine)
      if (n.tpe == Type.Unit) context.indent()
      n.function.referredEntity match
        case Some(reference) if reference.entity == symbols.Entity.print => 
          // This is the builtin print function, which should have exactly one argument
          // We choose the right function name depending on the type of its argument
          val functionName = n.arguments.head.value.tpe match
            case Type.Bool    => "print_b"
            case Type.String  => "print_s"
            case Type.Int     => "print_i"
            case Type.Float   => "print_f"
            case t => throw Error(s"cannot print type '${t}' in C")
          context.output ++= functionName
        case _ => n.function.visit(this)
      
      val args = for arg <- n.arguments
        yield context.writingToString(arg.value.visit(this)(using _))

      context.output += '('
      context.output.appendCommaSeparated(args) { _ ++= _ }
      context.output += ')'
      // Again, if the function returns Unit, we assume it's an independent instruction
      if (n.tpe == Type.Unit) context.output ++= ";\n"
    }

  override def visitPrefixApplication(n: ast.PrefixApplication)(using context: Context): Unit =
    context.consideringInitialization { context => 
        // Idea: if necessary, adapt the builtin function names in `transpiledReferenceTo`.
        n.function.visit(this)(using context)
        context.output += '('
        n.argument.visit(this)(using context)
        context.output += ')'
    }

  override def visitInfixApplication(n: ast.InfixApplication)(using context: Context): Unit =
    context.consideringInitialization { context =>
      n.function.referredEntity match
        // Handle equality
        case Some(symbols.EntityReference(symbols.Entity.equality, _)) =>
          context.output ++= "("
          n.lhs.visit(this)
          context.output ++= " == "
          n.rhs.visit(this)
          context.output ++= ")"
        // Handle inequality
        case Some(symbols.EntityReference(symbols.Entity.inequality, _)) =>
          context.output ++= "("
          n.lhs.visit(this)
          context.output ++= " != "
          n.rhs.visit(this)
          context.output ++= ")"
        case _ =>
          n.function.visit(this)
          context.output ++= "("
          n.lhs.visit(this)
          context.output ++= ", "
          n.rhs.visit(this)
          context.output ++= ")"
    }

  override def visitConditional(n: ast.Conditional)(using context: Context): Unit =
    // Declare a unique boolean variable and visit the condition with said variable as a pending binding
    context.indent()
    val condName = context.nextConditionName()
    context.output ++= s"bool ${condName};\n"
    context.withPendingBinding(condName, None)(n.condition.visit(this)(using _))

    // Actual conditional
    context.indent()
    context.output ++= s"if (${condName}) {\n"
    context.indentation += 1
    n.successCase.visit(this)
    context.indentation -= 1
    context.indent()
    context.output ++= "} else {\n"
    context.indentation += 1
    n.failureCase.visit(this)
    context.indentation -= 1
    context.indent()
    context.output ++= "}\n"

  override def visitMatch(n: ast.Match)(using context: Context): Unit =
    ???

  override def visitMatchCase(n: ast.Match.Case)(using context: Context): Unit =
    ???

  override def visitLet(n: ast.Let)(using context: Context): Unit =
    context.indent()
    context.output ++= "{\n"
    context.indentation += 1

    // Visit the binding.
    // It is not really necessary to clear the pending binding, but we do it preemptively
    context.clearingPendingBinding(_.inScope(n.binding.visit(this)(using _)))

    // Visit the expression.
    n.body.visit(this)

    context.indentation -= 1
    context.indent()
    context.output ++= "}\n"

  override def visitLambda(n: ast.Lambda)(using context: Context): Unit =
    ???

  override def visitParenthesizedExpression(
      n: ast.ParenthesizedExpression
  )(using context: Context): Unit =
    n.inner match
      // Strip parenthesis around these expressions
      case e: (ast.Conditional | ast.Match | ast.Let | ast.ParenthesizedExpression) =>
        e.visit(this)
      case _ =>
        context.consideringInitialization { context =>
            context.output += '('
            n.inner.visit(this)
            context.output += ')'
        }

  override def visitAscribedExpression(
      n: ast.AscribedExpression
  )(using context: Context): Unit =
    context.consideringInitialization { context => 
      context.output ++= s"(${transpiledType(n.ascription.tpe)}) "
      n.inner.visit(this)
    }

  override def visitTypeIdentifier(n: ast.TypeIdentifier)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitRecordType(n: ast.RecordType)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitTypeApplication(n: ast.TypeApplication)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitArrow(n: ast.Arrow)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitSum(n: ast.Sum)(using context: Context): Unit =
    unexpectedVisit(n)

  override def visitParenthesizedType(n: ast.ParenthesizedType)(using context: Context): Unit =
    unexpectedVisit(n)

  private def visitPattern(n: ast.Pattern)(using context: Context): Unit =
    n match
      case b: Binding => visitBindingPattern(b)
      case v: ValuePattern => visitValuePattern(v)
      case r: RecordPattern => visitRecordPattern(r)
      case w: Wildcard => visitWildcard(w)
      case e: ErrorTree => visitError(e)
  
  private def visitBindingPattern(n: ast.Binding)(using context: Context): Unit = 
    ???

  override def visitValuePattern(n: ast.ValuePattern)(using context: Context): Unit =
    n.value.visit(this)

  override def visitRecordPattern(n: ast.RecordPattern)(using context: Context): Unit =
    // Write the unique identifier for this record, which depends solely on its type.
    ???

  override def visitWildcard(n: ast.Wildcard)(using context: Context): Unit =
    ???
    

  override def visitError(n: ast.ErrorTree)(using context: Context): Unit =
    unexpectedVisit(n)
  
object CPrinter:

  /** The local state of a transpilation to C.
   *
   *  @param indentation The current identation to add before newlines.
   */
  final class Context(var indentation: Int = 0):

    /** The types that must be emitted in the program. */
    private var _typesToEmit = mutable.ListBuffer[symbols.Type.Record | symbols.Type.Sum]()

    /** The types that must be emitted in the program. */
    def typesToEmit: List[symbols.Type.Record | symbols.Type.Sum] = _typesToEmit.distinct.toList

    /** The helper functions to be emmitted to th program. (i.e. for conditionals) */
    private var _helperFunctions = mutable.Set[String]()

    /** The helper functions to be emmitted to th program. (i.e. for conditionals) */
    def helperFunctions: Set[String] = _helperFunctions.toSet

    /** Unique identifier for the next helper function. */
    def nextHelperFunctionId: Int = _helperFunctions.size

    /** The (partial) result of the transpilation. */
    private var _output = StringBuilder()

    /** The (partial) result of the transpilation. */
    def output: StringBuilder = _output

    /** `true` iff the transpiler is processing top-level symbols. */
    private var _isTopLevel = true

    /** `true` iff the transpiler is processing top-level symbols. */
    def isTopLevel: Boolean = _isTopLevel

    /** Adds `t` to the set of types that are used by the transpiled program. */
    def registerUse(t: symbols.Type.Record): Unit =
      if t != symbols.Type.Unit then
        t.fields.map(_.value).map { field =>
          field match
            case r: Type.Record => registerUse(r)
            case _ => ()
        }
        _typesToEmit += t
    
    def registerUse(t: symbols.Type.Sum): Unit = 
      t.members.map { member =>
        member match
          case r: Type.Record => registerUse(r)
          case s: Type.Sum => registerUse(s)
          case _ => ()
      }
      _typesToEmit += t

    /** Adds `fun` to the set of helper functions used by the transpiled program. */
    def registerHelperFunction(fun: String): Unit =
      _helperFunctions.add(fun)

    /** Returns `action` applied on `this` where `output` has been exchanged with `o`. */
    def swappingOutputBuffer[R](o: StringBuilder)(action: Context => R): R =
      val old = _output
      _output = o
      try action(this) finally _output = old
    
    /** Applies `action` on `this`, writing to a `String` instead of `output`. Returns said `String`.*/
    def writingToString[R](action: Context => R): String =
      val old = _output
      _output = StringBuilder()
      try
        action(this)
        _output.toString
      finally
        _output = old


    /** Returns `action` applied on `this` where `isTopLevel` is `false`. */
    def inScope[R](action: Context => R): R =
      val tl = _isTopLevel
      _isTopLevel = false
      try action(this) finally
        _isTopLevel = tl
    
    def indent() = _output ++= "\t" * indentation

    /** Initializations of global variables that are declared at the top level, but must be initialized in `main` */
    private val _globals = StringBuilder()

    /** Initializations of global variables that are declared at the top level, but must be initialized in `main` */
    def globals = _globals

    case class PendingBinding(name: String, tpe: Option[String])

    /** `Some(...)` if this context has a binding pending to be initialized, `None` if there is none. */
    private var _binding : Option[PendingBinding] = None 

    /** `Some(...)` if this context has a binding pending to be initialized, `None` if there is none. */
    def pendingBinding : Option[PendingBinding] = _binding
    
    /** Applies `action` on `this` without the current pending binding, if any.*/
    def clearingPendingBinding[R](action: Context => R): R =
      val b = _binding 
      _binding = None
      try action(this) finally
        _binding = b

    /** Applies `action` on `this` considering the variable identified by `name` as a pending binding.*/
    def withPendingBinding[R](name: String, tpe: Option[String])(action: Context => R): R =
      val b = _binding 
      _binding = Some(PendingBinding(name, tpe))
      try action(this) finally
        _binding = b
    
    /**
     *  Applies `action` on `this`, and uses it to initialize the pending binding if there is one.
     *  However, it clears said pending binding before executing `action`.
     *  Note that `action` must include a series of writes to `output`.
     */
    def consideringInitialization[R](action: Context => R): R =
      _binding match
        case None => action(this)
        case Some(PendingBinding(name, tpe)) =>
          indent()
          val typecast = tpe.map(t => s"(${t}) ").getOrElse("")
          _output ++= s"${name} = ${typecast}"
          val b = _binding
          _binding = None
          try
            action(this)
          finally
            _binding = b
            _output ++= ";\n"
    
    /** The id of the next condition variable. */
    private var _nextConditionId = 0

    def nextConditionName(): String =
      val id = _nextConditionId
      _nextConditionId += 1
      s"Cond_${id}"

  end Context

end CPrinter

extension (self: StringBuilder) def appendCommaSeparated[T](ls: Seq[T])(
    reduce: (StringBuilder, T) => Unit
): Unit =
    var f = true
    for l <- ls do
      if f then f = false else self ++= ", "
      reduce(self, l)
