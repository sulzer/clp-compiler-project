//BEGIN Hello World should print Hello World (1pt)
let main = print("Hello World!")

//OUT
Hello World!
//END

//BEGIN Printing the content of a top level let should print the value (1pt)
let x = "koala"
let main = print(x)

//OUT 
koala
//END

//BEGIN Printing the return value of a function with no arguments returning a string should print the string (1pt)
fun f() -> String {
    "koala42"
}
let main = print(f())

//OUT
koala42
//END

//BEGIN Printing the return value of a function with no arguments returning an integer should print the integer (1pt)
fun f() -> Int {
    42
}
let main = print(f())

//OUT
42
//END

//BEGIN Printing a value in a record, indexed by index, should print the value (2pt)
let r = #record("koala", 42)
let main = print(r.0)

//OUT
koala
//END

//BEGIN Printing a value in a record, indexed by name, should print the value (2pt)
let r = #record(name:"koala", age:42)
let main = print(r.age)

//OUT
42
//END


//BEGIN Accessing a field of a record inside a record should work
let r = #record(name:"koala", age:42)
let r2 = #record(r)
let r3 = r2.0
let main = print(r3.age)
//OUT
42
//END

//BEGIN Multiple records can be accessed (2pts)
let record1 = #record(foo: 42)
let record2 = #record(foo: 43)
let main = print(record1.foo + record2.foo)

//OUT
85
//END

//BEGIN Records fields are evaluated (2pts)
let record = #record(foo: (1 + 10))
let main = print(record.foo)

//OUT
11
//END

//BEGIN Record with multiple types
let a = 1
let r = #balloon(size: (a + 1), #description(text: "balloon"))
let size = r.size
let main = if (size < 3) then print((r.1).text) else print("Too big!")

//OUT
balloon
//END

//BEGIN Ascription and closed unions
let x: Int | String = 17
let y: Int | String | Float = 0.3
let r = let k : Int | Bool = false { if (2 < 5) then "1" else "2" }
let z = (1.0 @ Int)
let main = print(z)

//OUT
1
//END

//BEGIN Prime checking complex test
let x = 93187
fun oddDivisorsGEQthan(_ y: Int) -> Bool { if (y * y > x) then false else if x % y == 0 then true else oddDivisorsGEQthan(y + 2) }
fun isPrime() -> Bool { if (x == 2) then true else (if (x % 2 == 0) then false else !oddDivisorsGEQthan(3)) }
let main = if (isPrime()) then print("prime") else print("not prime")

//OUT
prime
//END