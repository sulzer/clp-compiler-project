#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>


void panic() {
    printf("panic\n");
    exit(-1);
}


// builtin functions
void print_s(char * s) {
    printf("%s\n", s);
}
void print_i(int i) {
    printf("%d\n", i);
}
void print_f(float f) {
    printf("%f\n", f);
}
void print_d(double d) {
    printf("%f\n", d);
}
void print_b(bool b) {
    printf("%s\n", b ? "true" : "false");
}

int iadd(int a, int b) { return a+b; }
int isub(int a, int b) { return a-b; }
int imul(int a, int b) { return a*b; }
int idiv(int a, int b) { return a/b; }
int irem(int a, int b) { return a%b; }
//int exit(int a) { exit(a); } already defined by stdlib.h
bool lnot(bool a) { return !a; }
bool land(bool a, bool b) { return a && b; }
bool lor(bool a, bool b) { return a || b; }
int ineg(int a) { return -a; }
int ishl(int a, int b) { return a << b; }
int ishr(int a, int b) { return a >> b; }
bool ilt(int a, int b) { return a < b; }
bool ile(int a, int b) { return a <= b; }
bool igt(int a, int b) { return a > b; }
bool ige(int a, int b) { return a >= b; }
int iinv(int a) { return ~a; }
int iand(int a, int b) { return a & b; }
int ior(int a, int b) { return a | b; }
int ixor(int a, int b) { return a ^ b; }
float fneg(float a) { return -a; }
float fadd(float a, float b) { return a + b; }
float fsub(float a, float b) { return a - b; }
float fmul(float a, float b) { return a * b; }
float fdiv(float a, float b) { return a / b; }
bool fle(float a, float b) { return a <= b; }
bool flt(float a, float b) { return a < b; }
bool fgt(float a, float b) { return a > b; }
bool fge(float a, float b) { return a >= b; }
