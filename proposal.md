# CS-320: Project Proposal

## 1. Extensions

- Closed Unions (1 pt)

- C (2 pts)

## 2. Parts of the compiler to modify

### 2.1. For Closed Unions

For this part we would add Closed Unions to our compiler.
In order to make this work, our transpiler to C will define and create the different union types that would be represented as `struct`s which would include a `union` and tag that would hold the actual type.
Additionally, we will modify both the `Typer` and the `Transpiler` in order to handle the new closed union types.
The main target for the `Typer` is the `visitSum` function and the same goes for the `Transpiler`.
In the case of our `Parser`, closed unions should already be supported but there might be changes needed.

### 2.2. For C

No parts of the compiler should need to be modified, but we would have to add a transpiler to C. This will include adding traits such as `CTree` or `CType` to represent C constructs as Scala objects and classes,
as well as a `CGenerator` tree visitor that would produce a C module after visiting an alpine `Tree`.

## 3. Examples of code

### 3.1. For Closed Unions

Here is an expression that we will be able to compile to C:

```
let x: Int | String | Float = "hello"

let y: Int | String = match x {
    case let u: Int then u + 1;
    case let w: Float then w.toString();
    case _ then "Must be a string"
}
```

### 3.2. For C
All the features supported by the language during the first five labs, including pattern matching, meaning our compiler
should correctly compile to C all alpine files from all labs.
Furthermore, it should be able to correctly compile closed unions.

Here's example of code that will be supported by our compiler is:

```
let x = 1
let y = let r = 1 { r * 3 + x }
let main = print(y)
```
