#!/bin/bash
FILENAME=$1
if [ ! -d res/out ]
then
	mkdir res/out
fi
echo "Running transpiler"
# Ignore all lines that start with '['
sbt "run -s res/in/${FILENAME}.al" | grep -v '^\[' > res/out/${FILENAME}.c
echo "Compiling output.c"
gcc -I. res/out/${FILENAME}.c -o res/out/${FILENAME}
echo "Running output"
echo "----------------"
./res/out/${FILENAME}
echo "----------------"

