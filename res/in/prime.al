let x = 93187
fun oddDivisorsGEQthan(_ y: Int) -> Bool { if (y * y > x) then false else if x % y == 0 then true else oddDivisorsGEQthan(y + 2) }
fun isPrime() -> Bool { if (x == 2) then true else (if (x % 2 == 0) then false else !oddDivisorsGEQthan(3)) }
let main = if (isPrime()) then print("prime") else print("not prime")
