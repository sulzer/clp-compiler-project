let x: Int | String = 17
let y: Int | String | Float = 0.3
let r = let k : Int | Bool = false { if (2 < 5) then "1" else "2" }
let z = (1.0 @ Int)
let main = print(z)
