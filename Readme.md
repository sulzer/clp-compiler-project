# CLP Project

## Usage

### Scala tests

The whole suite of tests ca be ran with `sbt test` or the transpiler tests using `sbt testOnly *.transpiler.*`. 
Note that `gcc` is required for C compilation.

### Compiling and running individual alpine files

To compile and run individual examples, you can add your alpine file to the `res/in` directory.
Assuming your file is named `myfile.al`, you can then run `./compile-and-run.sh myfile` to compile and run the file.
The output of the transpiler will then be  `res/out/myfile.c`.

## Implementation

### C

One of the key differences between Alpine and C is that expressions such as Alpine conditionals, Alpine `let` expressions, and Alpine `match` expressions can
return values, whereas their equivalents in C can't.

For instance, the following Alpine code
```
let x = let y = 1 { if (y < 5) then "foo" else "bar" }
```
can't be directly translated to C:
```
char * x = {
	int y = 1;
	if (y < 5) {
		"foo"; // ???
	} else {
		"bar"; // ???
	}
}
```

Instead, in this case, we should delegate the initialization of `x` to both branches of the conditional:

```
char * x;
{
	int y = 1;
	if (y < 5) {
		x = "foo;
	} else {
		x = "bar";
	}
}
```

To achieve that, the context keeps track of the current binding pending to be initialized, and when we visit a node that is a valid initializer in C,
we call the function `context.consideringInitialization` to visit the node, and initialize the pending variable (if there is one) to the result of the visit.

This construct is used almost everywhere.

For instance, when evaluating conditions, the compiler declares a boolean variable `Cond_i` (where `i` is a unique id), and the condition is visited
with `Cond_i` as the pending binding.
```
bool Cond_i;

// Visit initializer for Cond_i

if (Cond_i) {
	...
} else {
	...
}
```

A similar idea should have been user to evaluate the arguments for a function application, but this was not possible due to time constraints.


### Closed Unions

For Closed Unions we build on top of the C transpiler, we did not have to modify anything in the parser as for closed unions it already gave an ast of type Sum. However for the Types, as previously we had defined closed unions only for records, we just expanded the definition such that it could include other types like built in types like String or Int. Additionally the typer and the solver had to be changed to be able to handle this. The biggest change had to be done to the C printer so that for each closed union a new C structure could be printed out.

For example

```
let x: Int | String = 0
let y = match x {
  case let u: Int then u
  case _ then 0
}
```

transpiles to the following C code

```
typedef struct {
  union { int32_t intVal, const char* strVal } payload;
  int8_t tag;
} IntOrString;

IntOrString x;
x.tag = 0; // This wasn't implemented because of time constraints either, but it's basically a single line of code.
x.payload.intValue = 0;

// Pattern matching is not really implemented yet
int y; 
switch (x.tag) {
    case 0:
        y = x.payload.intValue;
        break;
    default:
        y = 0;
        break;
}
```

To see these new features we have added additional tests that test closed unions at the end of the TyperTest.scala file.

## Missing features

There are some features of the language that are not yet supported by the last stage of our pipeline, mainly due to time constraints:

- Applications with conditionals or let expressions in their arguments.
- Lambdas.
- Ascriptions (only trivial ascriptions work, but, among other things, it doesn't work with closed unions)
- Match expressions.
